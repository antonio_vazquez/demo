package com.romo.demo.controller;

import com.romo.demo.model.db.Articulo;
import com.romo.demo.repository.ArticuloRepository;
import com.romo.demo.services.db.ArticuloService;
import com.romo.demo.services.db.CompaniaService;
import com.romo.demo.services.db.TipoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.security.Principal;
import java.util.List;

@Controller
@RequestMapping("/articulo")
public class ArticuloController {


    @Autowired
    private ArticuloRepository articuloRepository;

    @Autowired
    private ArticuloService articuloService;

    @Autowired
    private CompaniaService companiaService;

    @Autowired
    private TipoService tipoService;

    @RequestMapping
    public String index(Model model) throws Exception {
        model.addAttribute("entities", articuloService.findAll());
        return "articulo/index";
    }


    @RequestMapping("/create")
    public String create(Model model)  {
        model.addAttribute("form", new Articulo());
        model.addAttribute("compania", companiaService.findAll());
        model.addAttribute("tipo", tipoService.findAll());
       return "articulo/create";
    }


    @PostMapping("/create")
    public String create(@Validated @ModelAttribute("form") Articulo articulo, final BindingResult bindingResult, Model model, RedirectAttributes redirectAttributes, Principal principal) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("form", new Articulo());
            model.addAttribute("compania", companiaService.findAll());
            model.addAttribute("tipo", tipoService.findAll());
            return "articulo/create";
        }

        List<Articulo> articuloList = articuloService.findAll();

        for (Articulo a : articuloList){
            if (a.getNumero().equals(articulo.getNumero())){
                redirectAttributes.addFlashAttribute("FAIL_MESSAGE", "Duplicate number is not allowed!!!");
                return "redirect:/articulo";
            }
        }

        Articulo insert = articuloRepository.save(articulo);

        if (insert !=  null && insert.getId() > 0) {
            redirectAttributes.addFlashAttribute("SUCCESS_MESSAGE", "Item successfully registered");
        } else {
            redirectAttributes.addFlashAttribute("FAIL_MESSAGE", "Could not register successfully, please try again later");
        }
        return "redirect:/articulo";
    }

    @GetMapping("/update/{id}")
    public String update(Model model, @PathVariable("id") Long id, HttpServletRequest request) {
            model.addAttribute("form", articuloRepository.findById(id));
        model.addAttribute("compania", companiaService.findAll());
        model.addAttribute("tipo", tipoService.findAll());
            return "articulo/update";
    }


    @PostMapping("/update")
    public String articuloUpdate(Model model, @Validated @ModelAttribute("form") Articulo form, final BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            return "articulo/update";
        }
        Articulo update = articuloService.update(form);
        if (update !=  null && update.getId() > 0) {
            redirectAttributes.addFlashAttribute("UPDATE_MESSAGE", "Registry updated successfully");
        } else {
            redirectAttributes.addFlashAttribute("FAIL_MESSAGE", "Could not register successfully, please try again later");
        }
        return "redirect:/articulo";
    }



    @RequestMapping("/delete/{id}")
    public String delete(Model model, @PathVariable("id") Long id, RedirectAttributes redirectAttributes) {
        articuloService.delete(id);
        redirectAttributes.addFlashAttribute("DELETE_MESSAGE", "Record removed");
        return "redirect:/articulo";
    }

    @RequestMapping("/report")
    public void report(HttpServletResponse response) throws IOException {
        byte[] report = articuloService.buildReport();
        response.setContentType("application/ms-excel");
        response.setContentLength(report.length);
        response.setHeader("Content-Disposition", "attachment;filename= Item list" + ".xlsx");
        OutputStream out = response.getOutputStream();
        out.write(report);
    }


}
