package com.romo.demo.controller;

import com.romo.demo.form.ReciboForm;
import com.romo.demo.model.db.Articulo;
import com.romo.demo.model.db.Recibo;
import com.romo.demo.repository.ArticuloRepository;
import com.romo.demo.repository.ReciboRepository;
import com.romo.demo.services.db.ReciboService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.security.Principal;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/recibo")
public class ReciboController {


    @Autowired
    private ReciboRepository reciboRepository;

    @Autowired
    private ReciboService reciboService;

    @Autowired
    private ArticuloRepository articuloRepository;

    @RequestMapping
    public String index(Model model) throws Exception {
        model.addAttribute("form", new ReciboForm());
        model.addAttribute("entities", null);
        return "recibo/index";
    }

    @PostMapping
    public String index(Model model, @Validated @ModelAttribute("form") ReciboForm reciboForm, final BindingResult bindingResult) {
        if (!bindingResult.hasErrors()) {
            List<Recibo> reciboList = reciboService.queryBuilder(reciboForm.getDesde(), reciboForm.getHasta());
            model.addAttribute("form", new ReciboForm());
            model.addAttribute("entities", reciboList.stream().sorted(Comparator.comparing(Recibo::getRegister).reversed()).collect(Collectors.toList()));
        }
        return "recibo/index";
    }

    @RequestMapping("/create")
    public String create(Model model)  {
        model.addAttribute("form", new Recibo());
        model.addAttribute("articulo", articuloRepository.findAll().stream().sorted(Comparator.comparing(Articulo::getNumero)).collect(Collectors.toList()));
       return "recibo/create";
    }


    @PostMapping("/create")
    public String create(@Validated @ModelAttribute("form") Recibo recibo, final BindingResult bindingResult, Model model, RedirectAttributes redirectAttributes, Principal principal) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("form", new Recibo());
            model.addAttribute("articulo", articuloRepository.findAll().stream().sorted(Comparator.comparing(Articulo::getNumero)).collect(Collectors.toList()));
            return "recibo/create";
        }

        recibo.setRegister(new Date());
        recibo.setUsername(principal.getName());
        Recibo insert = reciboRepository.save(recibo);

        if (insert !=  null && insert.getId() > 0) {
            redirectAttributes.addFlashAttribute("SUCCESS_MESSAGE", "User successfully registered");
        } else {
            redirectAttributes.addFlashAttribute("FAIL_MESSAGE", "Could not register successfully, please try again later");
        }
        return "redirect:/recibo";
    }

    @GetMapping("/update/{id}")
    public String update(Model model, @PathVariable("id") Long id, HttpServletRequest request) {
            model.addAttribute("form", reciboRepository.findById(id));
            model.addAttribute("articulo", articuloRepository.findAll().stream().sorted(Comparator.comparing(Articulo::getNumero)).collect(Collectors.toList()));
            return "recibo/update";
    }


    @PostMapping("/update")
    public String reciboUpdate(Model model, @Validated @ModelAttribute("form") Recibo form, final BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            return "recibo/update";
        }
        Recibo update = reciboService.update(form);
        if (update !=  null && update.getId() > 0) {
            redirectAttributes.addFlashAttribute("UPDATE_MESSAGE", "Registry updated successfully");
        } else {
            redirectAttributes.addFlashAttribute("FAIL_MESSAGE", "Could not register successfully, please try again later");
        }
        return "redirect:/recibo";
    }



    @RequestMapping("/delete/{id}")
    public String delete(Model model, @PathVariable("id") Long id, RedirectAttributes redirectAttributes) {
        reciboService.delete(id);
        redirectAttributes.addFlashAttribute("DELETE_MESSAGE", "Record removed");
        return "redirect:/recibo";
    }

    @RequestMapping("/report")
    public void report(HttpServletResponse response) throws IOException {
        byte[] report = reciboService.buildReport();
        response.setContentType("application/ms-excel");
        response.setContentLength(report.length);
        response.setHeader("Content-Disposition", "attachment;filename= Receiving list" + ".xlsx");
        OutputStream out = response.getOutputStream();
        out.write(report);
    }


}
