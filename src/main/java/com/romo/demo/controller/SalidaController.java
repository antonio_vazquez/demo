package com.romo.demo.controller;

import com.romo.demo.form.SalidaForm;
import com.romo.demo.model.db.Articulo;
import com.romo.demo.model.db.Salida;
import com.romo.demo.repository.ArticuloRepository;
import com.romo.demo.repository.SalidaRepository;
import com.romo.demo.services.db.SalidaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.security.Principal;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/salida")
public class SalidaController {


    @Autowired
    private SalidaRepository salidaRepository;

    @Autowired
    private SalidaService salidaService;

    @Autowired
    private ArticuloRepository articuloRepository;

    @RequestMapping
    public String index(Model model) throws Exception {
        model.addAttribute("form", new SalidaForm());
        model.addAttribute("entities", null);
        return "salida/index";
    }

    @PostMapping
    public String index(Model model, @Validated @ModelAttribute("form") SalidaForm salidaForm, final BindingResult bindingResult) {
        if (!bindingResult.hasErrors()) {
            List<Salida> salidaList = salidaService.queryBuilder(salidaForm.getDesde(), salidaForm.getHasta());
            model.addAttribute("form", new SalidaForm());
            model.addAttribute("entities", salidaList);
        }
        return "salida/index";
    }


    @RequestMapping("/create")
    public String create(Model model)  {
        model.addAttribute("form", new Salida());
        model.addAttribute("articulo", articuloRepository.findAll().stream().sorted(Comparator.comparing(Articulo::getNumero)).collect(Collectors.toList()) );
       return "salida/create";
    }


    @PostMapping("/create")
    public String create(@Validated @ModelAttribute("form") Salida salida, final BindingResult bindingResult, Model model, RedirectAttributes redirectAttributes, Principal principal) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("form", new Salida());
            model.addAttribute("articulo", articuloRepository.findAll().stream().sorted(Comparator.comparing(Articulo::getNumero)).collect(Collectors.toList()));
            return "salida/create";
        }

        salida.setRegister(new Date());
        salida.setUsername(principal.getName());
        Salida insert = salidaRepository.save(salida);

        if (insert !=  null && insert.getId() > 0) {
            redirectAttributes.addFlashAttribute("SUCCESS_MESSAGE", "User successfully registered");
        } else {
            redirectAttributes.addFlashAttribute("FAIL_MESSAGE", "Could not register successfully, please try again later");
        }
        return "redirect:/salida";
    }

    @GetMapping("/update/{id}")
    public String update(Model model, @PathVariable("id") Long id, HttpServletRequest request) {
            model.addAttribute("form", salidaRepository.findById(id));
            model.addAttribute("articulo", articuloRepository.findAll().stream().sorted(Comparator.comparing(Articulo::getNumero)).collect(Collectors.toList()));
            return "salida/update";
    }


    @PostMapping("/update")
    public String salidaUpdate(Model model, @Validated @ModelAttribute("form") Salida form, final BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            return "salida/update";
        }
        Salida update = salidaService.update(form);
        if (update !=  null && update.getId() > 0) {
            redirectAttributes.addFlashAttribute("UPDATE_MESSAGE", "Registry updated successfully");
        } else {
            redirectAttributes.addFlashAttribute("FAIL_MESSAGE", "Could not register successfully, please try again later");
        }
        return "redirect:/salida";
    }



    @RequestMapping("/delete/{id}")
    public String delete(Model model, @PathVariable("id") Long id, RedirectAttributes redirectAttributes) {
        salidaService.delete(id);
        redirectAttributes.addFlashAttribute("DELETE_MESSAGE", "Record removed");
        return "redirect:/salida";
    }

    @RequestMapping("/report")
    public void report(HttpServletResponse response) throws IOException {
        byte[] report = salidaService.buildReport();
        response.setContentType("application/ms-excel");
        response.setContentLength(report.length);
        response.setHeader("Content-Disposition", "attachment;filename= Shipping list" + ".xlsx");
        OutputStream out = response.getOutputStream();
        out.write(report);
    }



}
