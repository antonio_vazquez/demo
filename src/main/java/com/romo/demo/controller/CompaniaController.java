package com.romo.demo.controller;

import com.romo.demo.model.db.Compania;
import com.romo.demo.repository.CompaniaRepository;
import com.romo.demo.services.db.CompaniaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.security.Principal;

@Controller
@RequestMapping("/compania")
public class CompaniaController {


    @Autowired
    private CompaniaRepository companiaRepository;

    @Autowired
    private CompaniaService companiaService;

    @RequestMapping
    public String index(Model model) throws Exception {
        model.addAttribute("entities", companiaService.findAll());
        return "compania/index";
    }


    @RequestMapping("/create")
    public String create(Model model)  {
        model.addAttribute("form", new Compania());
       return "compania/create";
    }


    @PostMapping("/create")
    public String create(@Validated @ModelAttribute("form") Compania compania, final BindingResult bindingResult, Model model, RedirectAttributes redirectAttributes, Principal principal) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("form", new Compania());
            return "compania/create";
        }

         Compania insert = companiaRepository.save(compania);

        if (insert !=  null && insert.getId() > 0) {
            redirectAttributes.addFlashAttribute("SUCCESS_MESSAGE", "User successfully registered");
        } else {
            redirectAttributes.addFlashAttribute("FAIL_MESSAGE", "Could not register successfully, please try again later");
        }
        return "redirect:/compania";
    }

    @GetMapping("/update/{id}")
    public String update(Model model, @PathVariable("id") Long id, HttpServletRequest request) {
            model.addAttribute("form",  companiaRepository.findById(id));
            return "compania/update";
    }


    @PostMapping("/update")
    public String companiaUpdate(Model model, @Validated @ModelAttribute("form") Compania form, final BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            return "compania/update";
        }
        Compania update = companiaService.update(form);
        if (update !=  null && update.getId() > 0) {
            redirectAttributes.addFlashAttribute("UPDATE_MESSAGE", "Registry updated successfully");
        } else {
            redirectAttributes.addFlashAttribute("FAIL_MESSAGE", "Could not register successfully, please try again later");
        }
        return "redirect:/compania";
    }



    @RequestMapping("/delete/{id}")
    public String delete(Model model, @PathVariable("id") Long id, RedirectAttributes redirectAttributes) {
        companiaService.delete(id);
        redirectAttributes.addFlashAttribute("DELETE_MESSAGE", "Record removed");
        return "redirect:/compania";
    }

    @RequestMapping("/report")
    public void report(HttpServletResponse response) throws IOException {
        byte[] report = companiaService.buildReport();
        response.setContentType("application/ms-excel");
        response.setContentLength(report.length);
        response.setHeader("Content-Disposition", "attachment;filename= Company list" + ".xlsx");
        OutputStream out = response.getOutputStream();
        out.write(report);
    }


}
