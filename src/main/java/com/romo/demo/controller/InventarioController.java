package com.romo.demo.controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.romo.demo.form.InventarioForm;
import com.romo.demo.model.db.Articulo;
import com.romo.demo.model.logic.Inventario;
import com.romo.demo.repository.ArticuloRepository;
import com.romo.demo.services.db.ArticuloService;
import com.romo.demo.services.logic.InventarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.security.Principal;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/inventario")
public class InventarioController {


    @Autowired
    private ArticuloRepository articuloRepository;

    @Autowired
    private ArticuloService articuloService;

    @Autowired
    private InventarioService inventarioService;

    @RequestMapping
    public String index(Model model) throws Exception {
        model.addAttribute("entities", null);
        model.addAttribute("form", new InventarioForm());
        return "inventario/index";
    }

    @PostMapping
    public String index(Model model, @Validated @ModelAttribute("form") InventarioForm inventarioForm, final BindingResult bindingResult) {
        if (!bindingResult.hasErrors()) {
            List<Inventario> inventarioList = inventarioService.queryBuilder(inventarioForm.getNombre(),inventarioForm.getNumero(),inventarioForm.getCompania());
            model.addAttribute("form", new InventarioForm());
            model.addAttribute("entities", inventarioList);
        }
        return "inventario/index";
    }


    @RequestMapping("/create")
    public String create(Model model)  {
        model.addAttribute("form", new Inventario());
       return "inventario/create";
    }


    @PostMapping("/create")
    public String create(@Validated @ModelAttribute("form") Articulo articulo, final BindingResult bindingResult, Model model, RedirectAttributes redirectAttributes, Principal principal) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("form", new Articulo());
            return "articulo/create";
        }

        Articulo insert = articuloRepository.save(articulo);

        if (insert !=  null && insert.getId() > 0) {
            redirectAttributes.addFlashAttribute("SUCCESS_MESSAGE", "User successfully registered");
        } else {
            redirectAttributes.addFlashAttribute("FAIL_MESSAGE", "Could not register successfully, please try again later");
        }
        return "redirect:/articulo";
    }

    @GetMapping("/update/{id}")
    public String update(Model model, @PathVariable("id") Long id, HttpServletRequest request) {
            model.addAttribute("form", articuloRepository.findById(id));
            return "articulo/update";
    }


    @PostMapping("/update")
    public String articuloUpdate(Model model, @Validated @ModelAttribute("form") Articulo form, final BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            return "articulo/update";
        }
        Articulo update = articuloService.update(form);
        if (update !=  null && update.getId() > 0) {
            redirectAttributes.addFlashAttribute("UPDATE_MESSAGE", "Registry updated successfully");
        } else {
            redirectAttributes.addFlashAttribute("FAIL_MESSAGE", "Could not register successfully, please try again later");
        }
        return "redirect:/articulo";
    }



    @RequestMapping("/delete/{id}")
    public String delete(Model model, @PathVariable("id") Long id, RedirectAttributes redirectAttributes) {
        articuloService.delete(id);
        redirectAttributes.addFlashAttribute("DELETE_MESSAGE", "Record removed");
        return "redirect:/articulo";
    }

    @PostMapping(params = "report")
    public void report(@Validated @ModelAttribute("form") InventarioForm inventarioForm, final BindingResult bindingResult ,HttpServletResponse response) throws IOException {
        byte[] report = inventarioService.buildReport(inventarioForm.getNombre(),inventarioForm.getNumero(),inventarioForm.getCompania());
        response.setContentType("application/ms-excel");
        response.setContentLength(report.length);
        response.setHeader("Content-Disposition", "attachment;filename= Inventory list" + ".xlsx");
        OutputStream out = response.getOutputStream();
        out.write(report);
    }



}
