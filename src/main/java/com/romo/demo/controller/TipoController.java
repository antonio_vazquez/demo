package com.romo.demo.controller;

import com.romo.demo.model.db.Tipo;
import com.romo.demo.repository.TipoRepository;
import com.romo.demo.services.db.TipoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.security.Principal;

@Controller
@RequestMapping("/tipo")
public class TipoController {


    @Autowired
    private TipoRepository tipoRepository;

    @Autowired
    private TipoService tipoService;

    @RequestMapping
    public String index(Model model) throws Exception {
        model.addAttribute("entities", tipoService.findAll());
        return "tipo/index";
    }


    @RequestMapping("/create")
    public String create(Model model)  {
        model.addAttribute("form", new Tipo());
       return "tipo/create";
    }


    @PostMapping("/create")
    public String create(@Validated @ModelAttribute("form") Tipo tipo, final BindingResult bindingResult, Model model, RedirectAttributes redirectAttributes, Principal principal) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("form", new Tipo());
            return "tipo/create";
        }

         Tipo insert = tipoRepository.save(tipo);

        if (insert !=  null && insert.getId() > 0) {
            redirectAttributes.addFlashAttribute("SUCCESS_MESSAGE", "User successfully registered");
        } else {
            redirectAttributes.addFlashAttribute("FAIL_MESSAGE", "Could not register successfully, please try again later");
        }
        return "redirect:/tipo";
    }

    @GetMapping("/update/{id}")
    public String update(Model model, @PathVariable("id") Long id, HttpServletRequest request) {
            model.addAttribute("form",  tipoRepository.findById(id));
            return "tipo/update";
    }


    @PostMapping("/update")
    public String tipoUpdate(Model model, @Validated @ModelAttribute("form") Tipo form, final BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            return "tipo/update";
        }
        Tipo update = tipoService.update(form);
        if (update !=  null && update.getId() > 0) {
            redirectAttributes.addFlashAttribute("UPDATE_MESSAGE", "Registry updated successfully");
        } else {
            redirectAttributes.addFlashAttribute("FAIL_MESSAGE", "Could not register successfully, please try again later");
        }
        return "redirect:/tipo";
    }



    @RequestMapping("/delete/{id}")
    public String delete(Model model, @PathVariable("id") Long id, RedirectAttributes redirectAttributes) {
        tipoService.delete(id);
        redirectAttributes.addFlashAttribute("DELETE_MESSAGE", "Record removed");
        return "redirect:/tipo";
    }

    @RequestMapping("/report")
    public void report(HttpServletResponse response) throws IOException {
        byte[] report = tipoService.buildReport();
        response.setContentType("application/ms-excel");
        response.setContentLength(report.length);
        response.setHeader("Content-Disposition", "attachment;filename= Type list" + ".xlsx");
        OutputStream out = response.getOutputStream();
        out.write(report);
    }


}
