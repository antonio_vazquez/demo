package com.romo.demo.form;

import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

public class SalidaForm {

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date desde;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date hasta;

    public Date getDesde() {
        return desde;
    }

    public void setDesde(Date desde) {
        this.desde = desde;
    }

    public Date getHasta() {
        return hasta;
    }

    public void setHasta(Date hasta) {
        this.hasta = hasta;
    }
}
