package com.romo.demo.form;

import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

public class InventarioForm {

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date desde;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date hasta;

    private String numero;

    private String compania;

    private String nombre;

    public Date getDesde() {
        return desde;
    }

    public void setDesde(Date desde) {
        this.desde = desde;
    }

    public Date getHasta() {
        return hasta;
    }

    public void setHasta(Date hasta) {
        this.hasta = hasta;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getCompania() {
        return compania;
    }

    public void setCompania(String compania) {
        this.compania = compania;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
