package com.romo.demo.services.db;

import com.querydsl.core.BooleanBuilder;
import com.romo.demo.model.db.*;
import com.romo.demo.model.logic.Inventario;
import com.romo.demo.repository.SalidaRepository;
import com.romo.demo.services.logic.InventarioService;
import org.apache.commons.collections4.IteratorUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.xssf.usermodel.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
@Transactional
public class SalidaService extends AbstractService<Salida, Long> {

    private static final Logger LOGGER = LoggerFactory.getLogger(SalidaService.class);

    @Autowired
    private SalidaRepository repository;

    @Autowired
    private SalidaService salidaService;

    @Autowired
    private ResourceLoader resourceLoader;


    @PostConstruct
    public void manage() {
        setRepository(repository);
        setLOGGER(LOGGER);
    }

    @Override
    public Salida update(Salida r) {
        Salida toUpdate = new Salida();
        try {
            toUpdate =  salidaService.findOne(r.getId());
            toUpdate.setArticulo(r.getArticulo());
            toUpdate.setQty(r.getQty());

        } catch (Exception e) {
            LOGGER.error("Ocurrio un error:[" + ExceptionUtils.getFullStackTrace(e) + "]");
        }
        return toUpdate;
    }

    public List<Salida> findAllByArticulo(Articulo articulo){
        List<Salida> salidaList = new ArrayList<>();
        salidaList = repository.findAllByArticulo(articulo);

        return salidaList;
    }

    public List<Salida> queryBuilder(String nombre, String numero, String compania) {
        try {
            QSalida salida = QSalida.salida;
            BooleanBuilder where = new BooleanBuilder();
            if (!StringUtils.isEmpty(nombre)) {
                where.and(salida.articulo.nombre.contains(nombre));
            }
            if (!StringUtils.isEmpty(numero)) {
                where.and(salida.articulo.numero.eq(numero));
            }
            if (!StringUtils.isEmpty(compania)) {
                where.and(salida.articulo.compania.nombre.contains(compania));
            }
            return IteratorUtils.toList(this.repository.findAll(where.getValue()).iterator());
        } catch (Exception e) {
            LOGGER.error("Ocurrio un error:[" + ExceptionUtils.getFullStackTrace(e) + "]");
            return null;
        }
    }

    public List<Salida> queryBuilder(Date desde, Date hasta) {
        try {
            QSalida salida = QSalida.salida;
            BooleanBuilder where = new BooleanBuilder();
            if (desde != null) {
                where.and(salida.register.after(desde));
            }
            if (hasta != null) {
                where.and(salida.register.before(hasta));
            }
            return IteratorUtils.toList(this.repository.findAll(where.getValue()).iterator());
        } catch (Exception e) {
            LOGGER.error("Ocurrio un error:[" + ExceptionUtils.getFullStackTrace(e) + "]");
            return null;
        }
    }

    public byte[] buildReport(){
        try {
            Resource resource = resourceLoader.getResource("classpath:static/assets/reports/shipping.xlsx");
            XSSFWorkbook book = new XSSFWorkbook(resource.getInputStream());
            XSSFCell cellTable;
            XSSFSheet sheetBase = book.getSheetAt(0);
            //***********************styles*********************
            XSSFCellStyle styleDate = book.createCellStyle();
            CreationHelper createHelper = book.getCreationHelper();
            styleDate.setDataFormat(createHelper.createDataFormat().getFormat("dd/mm/yyyy hh:mm:ss"));
            //**************************************************
            List<Salida> salidaList = salidaService.findAll();
            int rowIndex=2;
            XSSFRow baseRow;
            for (Salida s: salidaList){
                baseRow = sheetBase.createRow(rowIndex++);
                cellTable = baseRow.createCell(0);cellTable.setCellValue(s.getArticulo().getNumero());
                cellTable = baseRow.createCell(1);cellTable.setCellValue(s.getArticulo().getNombre());
                cellTable = baseRow.createCell(2);cellTable.setCellValue(s.getArticulo().getDescripcion());
                cellTable = baseRow.createCell(3);cellTable.setCellValue(s.getArticulo().getCompania().getNombre());
                cellTable = baseRow.createCell(4);cellTable.setCellValue(s.getQty());
                cellTable = baseRow.createCell(5);cellTable.setCellValue(s.getRegister());cellTable.setCellStyle(styleDate);
                cellTable = baseRow.createCell(6);cellTable.setCellValue(s.getUsername());
            }
            ByteArrayOutputStream report = new ByteArrayOutputStream();
            book.write(report);
            return report.toByteArray();

        }catch (Exception e){
            LOGGER.error("Ocurrio un error en la generación del reporte de Excel [");
        }
        return null;
    }

}
