package com.romo.demo.services.db;

import com.querydsl.core.BooleanBuilder;
import com.romo.demo.model.db.Articulo;
import com.romo.demo.model.db.QRecibo;
import com.romo.demo.model.db.Recibo;
import com.romo.demo.model.logic.Inventario;
import com.romo.demo.repository.ReciboRepository;
import com.romo.demo.services.logic.InventarioService;
import org.apache.commons.collections4.IteratorUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.xssf.usermodel.*;
import org.graalvm.compiler.lir.alloc.lsra.LinearScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
@Transactional
public class ReciboService extends AbstractService<Recibo, Long> {

    private static final Logger LOGGER = LoggerFactory.getLogger(ReciboService.class);

    @Autowired
    private ReciboRepository repository;

    @Autowired
    private ReciboService reciboService;

    @Autowired
    private ResourceLoader resourceLoader;


    @PostConstruct
    public void manage() {
        setRepository(repository);
        setLOGGER(LOGGER);
    }

    @Override
    public Recibo update(Recibo r) {
        Recibo toUpdate = new Recibo();
        try {
            toUpdate =  reciboService.findOne(r.getId());
            toUpdate.setArticulo(r.getArticulo());
            toUpdate.setQty(r.getQty());

        } catch (Exception e) {
            LOGGER.error("Ocurrio un error:[" + ExceptionUtils.getFullStackTrace(e) + "]");
        }
        return toUpdate;
    }

    public List<Recibo> findAllByArticulo(Articulo articulo){

        List<Recibo> reciboList = new ArrayList<>();

        reciboList = repository.findAllByArticulo(articulo);

        return reciboList;
    }

    public List<Recibo> queryBuilder(String nombre, String numero, String compania) {
        try {
            QRecibo recibo = QRecibo.recibo;
            BooleanBuilder where = new BooleanBuilder();
            if (!StringUtils.isEmpty(nombre)) {
                where.and(recibo.articulo.nombre.contains(nombre));
            }
            if (!StringUtils.isEmpty(numero)) {
                where.and(recibo.articulo.numero.eq(numero));
            }
            if (!StringUtils.isEmpty(compania)) {
                where.and(recibo.articulo.compania.nombre.contains(compania));
            }
            return IteratorUtils.toList(this.repository.findAll(where.getValue()).iterator());
        } catch (Exception e) {
            LOGGER.error("Ocurrio un error:[" + ExceptionUtils.getFullStackTrace(e) + "]");
            return null;
        }
    }

    public List<Recibo> queryBuilder(Date desde, Date hasta) {
        try {
            QRecibo recibo = QRecibo.recibo;
            BooleanBuilder where = new BooleanBuilder();
            if (desde != null) {
                where.and(recibo.register.after(desde));
            }
            if (hasta != null) {
                where.and(recibo.register.before(hasta));
            }
            return IteratorUtils.toList(this.repository.findAll(where.getValue()).iterator());
        } catch (Exception e) {
            LOGGER.error("Ocurrio un error:[" + ExceptionUtils.getFullStackTrace(e) + "]");
            return null;
        }
    }

    public byte[] buildReport(){
        try {
            Resource resource = resourceLoader.getResource("classpath:static/assets/reports/receiving.xlsx");
            XSSFWorkbook book = new XSSFWorkbook(resource.getInputStream());
            XSSFCell cellTable;
            XSSFSheet sheetBase = book.getSheetAt(0);
            //***********************styles*********************
            XSSFCellStyle styleDate = book.createCellStyle();
            CreationHelper createHelper = book.getCreationHelper();
            styleDate.setDataFormat(createHelper.createDataFormat().getFormat("dd/mm/yyyy hh:mm:ss"));
            //**************************************************
            List<Recibo> reciboList = reciboService.findAll();
            int rowIndex=2;
            XSSFRow baseRow;
            for (Recibo r: reciboList){
                baseRow = sheetBase.createRow(rowIndex++);
                cellTable = baseRow.createCell(0);cellTable.setCellValue(r.getArticulo().getNumero());
                cellTable = baseRow.createCell(1);cellTable.setCellValue(r.getArticulo().getNombre());
                cellTable = baseRow.createCell(2);cellTable.setCellValue(r.getArticulo().getDescripcion());
                cellTable = baseRow.createCell(3);cellTable.setCellValue(r.getArticulo().getCompania().getNombre());
                cellTable = baseRow.createCell(4);cellTable.setCellValue(r.getQty());
                cellTable = baseRow.createCell(5);cellTable.setCellValue(r.getRegister());cellTable.setCellStyle(styleDate);
                cellTable = baseRow.createCell(6);cellTable.setCellValue(r.getUsername());
            }
            ByteArrayOutputStream report = new ByteArrayOutputStream();
            book.write(report);
            return report.toByteArray();

        }catch (Exception e){
            LOGGER.error("Ocurrio un error en la generación del reporte de Excel [");
        }
        return null;
    }

}
