package com.romo.demo.services.db;

import com.romo.demo.model.db.Articulo;
import com.romo.demo.model.db.Compania;
import com.romo.demo.repository.CompaniaRepository;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import java.io.ByteArrayOutputStream;
import java.util.List;

@Service
@Transactional
public class CompaniaService extends AbstractService<Compania, Long> {

    private static final Logger LOGGER = LoggerFactory.getLogger(CompaniaService.class);

    @Autowired
    private CompaniaRepository repository;

    @Autowired
    private CompaniaService companiaService;

    @Autowired
    private ResourceLoader resourceLoader;

    @PostConstruct
    public void manage() {
        setRepository(repository);
        setLOGGER(LOGGER);
    }

    @Override
    public Compania update(Compania r) {
        Compania toUpdate = new Compania();
        try {
            toUpdate =  companiaService.findOne(r.getId());
            toUpdate.setNombre(r.getNombre());
            toUpdate.setDescripcion(r.getDescripcion());

        } catch (Exception e) {
            LOGGER.error("Ocurrio un error:[" + ExceptionUtils.getFullStackTrace(e) + "]");
        }
        return toUpdate;
    }

    public byte[] buildReport(){
        try {
            Resource resource = resourceLoader.getResource("classpath:static/assets/reports/company.xlsx");
            XSSFWorkbook book = new XSSFWorkbook(resource.getInputStream());
            XSSFCell cellTable;
            XSSFSheet sheetBase = book.getSheetAt(0);
            List<Compania> companiaList = companiaService.findAll();
            int rowIndex=2;
            XSSFRow baseRow;
            for (Compania a: companiaList){
                baseRow = sheetBase.createRow(rowIndex++);
                cellTable = baseRow.createCell(0);cellTable.setCellValue(a.getNombre());
                cellTable = baseRow.createCell(1);cellTable.setCellValue(a.getDescripcion());
            }
            ByteArrayOutputStream report = new ByteArrayOutputStream();
            book.write(report);
            return report.toByteArray();

        }catch (Exception e){
            LOGGER.error("Ocurrio un error en la generación del reporte de Excel [");
        }
        return null;
    }

}
