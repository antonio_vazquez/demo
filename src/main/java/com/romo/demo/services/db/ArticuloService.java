package com.romo.demo.services.db;

import com.querydsl.core.BooleanBuilder;
import com.romo.demo.model.db.Articulo;
import com.romo.demo.model.db.QArticulo;
import com.romo.demo.model.db.QSalida;
import com.romo.demo.model.db.Salida;
import com.romo.demo.repository.ArticuloRepository;
import org.apache.commons.collections4.IteratorUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.xssf.usermodel.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import java.io.ByteArrayOutputStream;
import java.util.Date;
import java.util.List;

@Service
@Transactional
public class ArticuloService extends AbstractService<Articulo, Long> {

    private static final Logger LOGGER = LoggerFactory.getLogger(ArticuloService.class);

    @Autowired
    private ArticuloRepository repository;

    @Autowired
    private ArticuloService articuloService;

    @Autowired
    private ResourceLoader resourceLoader;

    @PostConstruct
    public void manage() {
        setRepository(repository);
        setLOGGER(LOGGER);
    }

    @Override
    public Articulo update(Articulo r) {
        Articulo toUpdate = new Articulo();
        try {
            toUpdate =  articuloService.findOne(r.getId());
            toUpdate.setNumero(r.getNumero());
            toUpdate.setNombre(r.getNombre());
            toUpdate.setDescripcion(r.getDescripcion());
            toUpdate.setCompania(r.getCompania());
            toUpdate.setTipo(r.getTipo());

        } catch (Exception e) {
            LOGGER.error("Ocurrio un error:[" + ExceptionUtils.getFullStackTrace(e) + "]");
        }
        return toUpdate;
    }

    public List<Articulo> queryBuilder(String nombre, String numero, String compania) {
        try {
            QArticulo articulo = QArticulo.articulo;
            BooleanBuilder where = new BooleanBuilder();
            if (!StringUtils.isEmpty(nombre)) {
                where.and(articulo.nombre.contains(nombre));
            }
            if (!StringUtils.isEmpty(numero)) {
                where.and(articulo.numero.eq(numero));
            }
            if (!StringUtils.isEmpty(compania)) {
                where.and(articulo.compania.nombre.contains(compania));
            }
            return IteratorUtils.toList(this.repository.findAll(where.getValue()).iterator());
        } catch (Exception e) {
            LOGGER.error("Ocurrio un error:[" + ExceptionUtils.getFullStackTrace(e) + "]");
            return null;
        }
    }

    public byte[] buildReport(){
        try {
            Resource resource = resourceLoader.getResource("classpath:static/assets/reports/item.xlsx");
            XSSFWorkbook book = new XSSFWorkbook(resource.getInputStream());
            XSSFCell cellTable;
            XSSFSheet sheetBase = book.getSheetAt(0);
            List<Articulo> articuloList = articuloService.findAll();
            int rowIndex=2;
            XSSFRow baseRow;
            for (Articulo a: articuloList){
                baseRow = sheetBase.createRow(rowIndex++);
                cellTable = baseRow.createCell(0);cellTable.setCellValue(a.getNumero());
                cellTable = baseRow.createCell(1);cellTable.setCellValue(a.getNombre());
                cellTable = baseRow.createCell(2);cellTable.setCellValue(a.getDescripcion());
                cellTable = baseRow.createCell(3);cellTable.setCellValue(a.getCompania().getNombre());
            }
            ByteArrayOutputStream report = new ByteArrayOutputStream();
            book.write(report);
            return report.toByteArray();

        }catch (Exception e){
            LOGGER.error("Ocurrio un error en la generación del reporte de Excel [");
        }
        return null;
    }

}
