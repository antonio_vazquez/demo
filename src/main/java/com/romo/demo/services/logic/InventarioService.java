package com.romo.demo.services.logic;

import com.querydsl.core.BooleanBuilder;
import com.romo.demo.model.db.Articulo;
import com.romo.demo.model.db.Recibo;
import com.romo.demo.model.db.Salida;
import com.romo.demo.model.logic.Inventario;
import com.romo.demo.repository.ArticuloRepository;
import com.romo.demo.services.db.AbstractService;
import com.romo.demo.services.db.ArticuloService;
import com.romo.demo.services.db.ReciboService;
import com.romo.demo.services.db.SalidaService;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.swing.*;
import javax.transaction.Transactional;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

@Service
public class InventarioService {

    private static final Logger LOGGER = LoggerFactory.getLogger(InventarioService.class);

    @Autowired
    private ReciboService reciboService;

    @Autowired
    private SalidaService salidaService;

    @Autowired
    private ArticuloService articuloService;


    @Autowired
    private ResourceLoader resourceLoader;

    @Autowired
    private InventarioService inventarioService;

    public List<Inventario> buildInventario(){

        List<Inventario> inventarioList = new ArrayList<>();
        Inventario inventario = new Inventario();
        List<Recibo> reciboList = reciboService.findAll();
        List<Salida> salidaList = salidaService.findAll();
        List<Articulo> articuloList = articuloService.findAll();
        Integer index=0, sumRecibo=0, sumSalida=0;
        for (Articulo a: articuloList){
            inventario = new Inventario();
            inventario.setArticulo(a);
            reciboList = reciboService.findAllByArticulo(a);
            for (Recibo recibo: reciboList){
                sumRecibo+=recibo.getQty();
            }
            salidaList = salidaService.findAllByArticulo(a);
            for (Salida salida: salidaList){
                sumSalida+=salida.getQty();
            }
            inventario.setEntradas(sumRecibo);
            inventario.setSalidas(sumSalida);
            inventario.setTotal(sumRecibo-sumSalida);
            inventarioList.add(index,inventario);
            index++;sumRecibo=0;sumSalida=0;
        }

/*        for(Articulo a: articuloList){
            inventario = new Inventario();
            inventario.setArticulo(a);
            inventarioList.add(inventario);
        }
        for(Inventario i: inventarioList){
            List<Recibo> rList = reciboService.findAllByArticulo(i.getArticulo());
            for (Recibo r: rList){
                sumRecibo+=r.getQty();
            }
            List<Salida> sList = salidaService.findAllByArticulo(i.getArticulo());
            for (Salida s: sList){
                sumSalida+=s.getQty();
            }
            inventario.setArticulo(i.getArticulo());
            inventario.setEntradas(sumRecibo);
            inventario.setSalidas(sumSalida);
            inventario.setTotal(sumRecibo-sumSalida);
            inventarioList.add(inventario);
        }*/


        return inventarioList;
    }

    public List<Inventario> queryBuilder(String nombre, String numero, String compania) {
        try {
            List<Inventario> inventarioList = new ArrayList<>();
            List<Inventario> inventarioListFinal = new ArrayList<>();
            Inventario inventario = new Inventario();
            List<Recibo> reciboList = reciboService.queryBuilder(nombre, numero, compania);
            List<Salida> salidaList = salidaService.queryBuilder(nombre, numero, compania);
            List<Articulo> articuloList = articuloService.queryBuilder(nombre, numero, compania);
            Integer index=0, sumRecibo=0, sumSalida=0;
            for (Articulo a: articuloList){
                inventario = new Inventario();
                inventario.setArticulo(a);
                reciboList = reciboService.findAllByArticulo(a);
                for (Recibo recibo: reciboList){
                    sumRecibo+=recibo.getQty();
                }
                salidaList = salidaService.findAllByArticulo(a);
                for (Salida salida: salidaList){
                    sumSalida+=salida.getQty();
                }
                inventario.setEntradas(sumRecibo);
                inventario.setSalidas(sumSalida);
                inventario.setTotal(sumRecibo-sumSalida);
                inventarioList.add(index,inventario);
                index++;sumRecibo=0;sumSalida=0;
            }



            return inventarioList;
        } catch (Exception e) {
            LOGGER.error("Ocurrio un error:[" + ExceptionUtils.getFullStackTrace(e) + "]");
            return null;
        }
    }

    public byte[] buildReport(String nombre, String numero, String compania){
        try {
            Resource resource = resourceLoader.getResource("classpath:static/assets/reports/inventory.xlsx");
            XSSFWorkbook book = new XSSFWorkbook(resource.getInputStream());
            XSSFCell cellTable;
            XSSFSheet sheetBase = book.getSheetAt(0);
            List<Inventario> inventarioList = inventarioService.queryBuilder(nombre,numero,compania);
            int rowIndex=2;
            XSSFRow baseRow;
            for (Inventario i: inventarioList){
                baseRow = sheetBase.createRow(rowIndex++);
                cellTable = baseRow.createCell(0);cellTable.setCellValue(i.getArticulo().getNumero());
                cellTable = baseRow.createCell(1);cellTable.setCellValue(i.getArticulo().getNombre());
                cellTable = baseRow.createCell(2);cellTable.setCellValue(i.getArticulo().getDescripcion());
                cellTable = baseRow.createCell(3);cellTable.setCellValue(i.getArticulo().getCompania().getNombre());
                cellTable = baseRow.createCell(4);cellTable.setCellValue(i.getEntradas());
                cellTable = baseRow.createCell(5);cellTable.setCellValue(i.getSalidas());
                cellTable = baseRow.createCell(6);cellTable.setCellValue(i.getTotal());
            }
            ByteArrayOutputStream report = new ByteArrayOutputStream();
            book.write(report);
            return report.toByteArray();

        }catch (Exception e){
            LOGGER.error("Ocurrio un error en la generación del reporte de Excel [");
        }
        return null;
    }

}
