package com.romo.demo.services.logic;

import com.romo.demo.model.db.Usuario;
import com.romo.demo.model.logic.AppUsuario;
import com.romo.demo.services.db.UsuarioService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service("userDetailService")
public class UserDetailServiceImpl implements UserDetailsService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserDetailServiceImpl.class);

    @Autowired
    private UsuarioService usuarioService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Usuario usuario = usuarioService.findByCorreo(username);
        List<GrantedAuthority> setAuths = null;
        if (usuario != null && usuario.getId() > 0) {
            //usuarioService.update(usuario);
            setAuths = new ArrayList<GrantedAuthority>();

            setAuths.add(new SimpleGrantedAuthority(usuario.getRol().getNombre()));
        }
        return new AppUsuario(
                usuario.getCorreo(),
                usuario.getContasena(),
                true,
                true,
                true,
                true,
                setAuths,
                String.format("%s %s %s", usuario.getNombre(), usuario.getNombre(), usuario.getNombre()),
                null, null);
    }

}
