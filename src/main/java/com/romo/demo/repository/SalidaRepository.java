package com.romo.demo.repository;

import com.romo.demo.model.db.Articulo;
import com.romo.demo.model.db.Recibo;
import com.romo.demo.model.db.Salida;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

import java.util.List;

public interface SalidaRepository extends JpaRepository<Salida, Long>, QuerydslPredicateExecutor<Salida> {
    
List<Salida> findAllByArticulo(Articulo articulo);

}
