package com.romo.demo.repository;

import com.romo.demo.model.db.Rol;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface RolRepository extends JpaRepository<Rol, Long>, QuerydslPredicateExecutor<Rol> {


}
