package com.romo.demo.repository;

import com.romo.demo.model.db.Tipo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface TipoRepository extends JpaRepository<Tipo, Long>, QuerydslPredicateExecutor<Tipo> {


}
