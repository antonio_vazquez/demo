package com.romo.demo.repository;

import com.romo.demo.model.db.Articulo;
import com.romo.demo.model.db.Recibo;
import com.romo.demo.model.db.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

import java.util.List;

public interface ReciboRepository extends JpaRepository<Recibo, Long>, QuerydslPredicateExecutor<Recibo> {
    
List<Recibo> findAllByArticulo(Articulo articulo);

}
