package com.romo.demo.repository;

import com.romo.demo.model.db.UsuarioPerfil;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface UsuarioPerfilRepository extends JpaRepository<UsuarioPerfil, Long>, QuerydslPredicateExecutor<UsuarioPerfil> {

}
