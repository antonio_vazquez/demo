package com.romo.demo.repository;

import com.romo.demo.model.db.Articulo;
import com.romo.demo.model.db.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface ArticuloRepository extends JpaRepository<Articulo, Long>, QuerydslPredicateExecutor<Articulo> {


}
