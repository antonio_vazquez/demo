package com.romo.demo.repository;

import com.romo.demo.model.db.Compania;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface CompaniaRepository extends JpaRepository<Compania, Long>, QuerydslPredicateExecutor<Compania> {


}
