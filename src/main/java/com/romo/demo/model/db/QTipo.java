package com.romo.demo.model.db;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QTipo is a Querydsl query type for Tipo
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QTipo extends EntityPathBase<Tipo> {

    private static final long serialVersionUID = -741227402L;

    public static final QTipo tipo = new QTipo("tipo");

    public final StringPath descripcion = createString("descripcion");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath nombre = createString("nombre");

    public QTipo(String variable) {
        super(Tipo.class, forVariable(variable));
    }

    public QTipo(Path<? extends Tipo> path) {
        super(path.getType(), path.getMetadata());
    }

    public QTipo(PathMetadata metadata) {
        super(Tipo.class, metadata);
    }

}

