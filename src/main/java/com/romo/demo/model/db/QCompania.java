package com.romo.demo.model.db;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QCompania is a Querydsl query type for Compania
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QCompania extends EntityPathBase<Compania> {

    private static final long serialVersionUID = -1503833642L;

    public static final QCompania compania = new QCompania("compania");

    public final StringPath descripcion = createString("descripcion");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath nombre = createString("nombre");

    public QCompania(String variable) {
        super(Compania.class, forVariable(variable));
    }

    public QCompania(Path<? extends Compania> path) {
        super(path.getType(), path.getMetadata());
    }

    public QCompania(PathMetadata metadata) {
        super(Compania.class, metadata);
    }

}

