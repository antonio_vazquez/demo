package com.romo.demo.model.db;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QRecibo is a Querydsl query type for Recibo
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QRecibo extends EntityPathBase<Recibo> {

    private static final long serialVersionUID = 583695528L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QRecibo recibo = new QRecibo("recibo");

    public final QArticulo articulo;

    public final StringPath descripcion = createString("descripcion");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final NumberPath<Integer> qty = createNumber("qty", Integer.class);

    public final DateTimePath<java.util.Date> register = createDateTime("register", java.util.Date.class);

    public final StringPath username = createString("username");

    public QRecibo(String variable) {
        this(Recibo.class, forVariable(variable), INITS);
    }

    public QRecibo(Path<? extends Recibo> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QRecibo(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QRecibo(PathMetadata metadata, PathInits inits) {
        this(Recibo.class, metadata, inits);
    }

    public QRecibo(Class<? extends Recibo> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.articulo = inits.isInitialized("articulo") ? new QArticulo(forProperty("articulo")) : null;
    }

}

