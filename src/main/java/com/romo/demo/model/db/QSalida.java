package com.romo.demo.model.db;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QSalida is a Querydsl query type for Salida
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QSalida extends EntityPathBase<Salida> {

    private static final long serialVersionUID = 608898762L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QSalida salida = new QSalida("salida");

    public final QArticulo articulo;

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final NumberPath<Integer> qty = createNumber("qty", Integer.class);

    public final DateTimePath<java.util.Date> register = createDateTime("register", java.util.Date.class);

    public final StringPath username = createString("username");

    public QSalida(String variable) {
        this(Salida.class, forVariable(variable), INITS);
    }

    public QSalida(Path<? extends Salida> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QSalida(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QSalida(PathMetadata metadata, PathInits inits) {
        this(Salida.class, metadata, inits);
    }

    public QSalida(Class<? extends Salida> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.articulo = inits.isInitialized("articulo") ? new QArticulo(forProperty("articulo")) : null;
    }

}

