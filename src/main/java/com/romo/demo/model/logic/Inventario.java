package com.romo.demo.model.logic;

import com.romo.demo.model.db.Articulo;

import javax.persistence.criteria.CriteriaBuilder;

public class Inventario {

    private Articulo articulo;

    private Integer entradas;

    private Integer salidas;

    private Integer total;

    public Articulo getArticulo() {
        return articulo;
    }

    public void setArticulo(Articulo articulo) {
        this.articulo = articulo;
    }

    public Integer getEntradas() {
        return entradas;
    }

    public void setEntradas(Integer entradas) {
        this.entradas = entradas;
    }

    public Integer getSalidas() {
        return salidas;
    }

    public void setSalidas(Integer salidas) {
        this.salidas = salidas;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }
}
