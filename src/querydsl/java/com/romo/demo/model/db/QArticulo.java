package com.romo.demo.model.db;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QArticulo is a Querydsl query type for Articulo
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QArticulo extends EntityPathBase<Articulo> {

    private static final long serialVersionUID = -2132937443L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QArticulo articulo = new QArticulo("articulo");

    public final QCompania compania;

    public final StringPath descripcion = createString("descripcion");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath nombre = createString("nombre");

    public final StringPath numero = createString("numero");

    public final QTipo tipo;

    public QArticulo(String variable) {
        this(Articulo.class, forVariable(variable), INITS);
    }

    public QArticulo(Path<? extends Articulo> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QArticulo(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QArticulo(PathMetadata metadata, PathInits inits) {
        this(Articulo.class, metadata, inits);
    }

    public QArticulo(Class<? extends Articulo> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.compania = inits.isInitialized("compania") ? new QCompania(forProperty("compania")) : null;
        this.tipo = inits.isInitialized("tipo") ? new QTipo(forProperty("tipo")) : null;
    }

}

